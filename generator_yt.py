import requests
import xml.etree.ElementTree as ET

# URL del canal de YouTube RSS
RSS_URL = "https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg"

# Función para descargar el RSS
def fetch_rss(url):
    response = requests.get(url)
    response.raise_for_status()  # Asegura que la solicitud fue exitosa
    return response.content

# Función para generar la página HTML
def generate_html(root):
    html_content = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>CursosWeb YouTube Videos</title>
    </head>
    <body>
        <h1>Videos del canal CursosWeb</h1>
        <ul>
    """
    for entry in root.findall('{http://www.w3.org/2005/Atom}entry'):
        title = entry.find('{http://www.w3.org/2005/Atom}title').text
        link = entry.find('{http://www.w3.org/2005/Atom}link').attrib['href']
        html_content += f'<li><a href="{link}" target="_blank">{title}</a></li>\n'

    html_content += """
        </ul>
    </body>
    </html>
    """
    return html_content

# Descargar y parsear el RSS
rss_content = fetch_rss(RSS_URL)
root = ET.fromstring(rss_content)

# Generar la página HTML
html_content = generate_html(root)

# Guardar el HTML en un archivo
with open("cursosweb_videos.html", "w", encoding="utf-8") as file:
    file.write(html_content)

print("Archivo HTML generado: cursosweb_videos.html")
